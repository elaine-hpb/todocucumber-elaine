Feature: the Todo can be retrieved
  Scenario: client makes call to GET /todos
   	Given todo rest endpoint of "13.212.185.195:8080" is up
    When the client calls /todos
    Then the client receives status code of 200